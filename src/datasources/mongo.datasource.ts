import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'mongo',
  connector: 'mongodb',
  url:
    process.env.DB_URL ||
    'mongodb+srv://admin:adminSample@cluster0.zdezw.mongodb.net/nuclio?retryWrites=true&w=majority',

  host: process.env.DB_HOST || 'localhost',
  port: 27017,
  user: process.env.DB_USER || 'admin',
  password: process.env.DB_PASSWORD || 'adminSample',
  database: process.env.DB_DATABASE || 'nuclio',
  useNewUrlParser: true,
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MongoDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mongo';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.mongo', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
