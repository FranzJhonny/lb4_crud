import {Entity, model, property} from '@loopback/repository';

@model()
export class SaleIteam extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'number',
    required: true,
  })
  amount: number;


  constructor(data?: Partial<SaleIteam>) {
    super(data);
  }
}

export interface SaleIteamRelations {
  // describe navigational properties here
}

export type SaleIteamWithRelations = SaleIteam & SaleIteamRelations;
